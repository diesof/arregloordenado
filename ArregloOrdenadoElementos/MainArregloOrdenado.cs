﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloOrdenadoElementos
{
    class MainArregloOrdenado
    {
        static void Main(string[] args)
        {
            ArregloOrdenado a = new ArregloOrdenado();

            a.SolicitarLongitudArreglo();

            a.IngresarDatosUsuario();

            a.Menu();
        }
    }
}