﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloOrdenadoElementos
{
    class ArregloOrdenado
    {
        //Variables
        private int test;
        private int[] edades;
        private readonly List<object> lista = new List<object>() { 6, 8, 9, 10, 'c', "sofi", 5.5 };
        private int opcionOrden;

        //Método que solicita la longitud del arreglo, esto quiere decir solicita la cantidad de edades que se van a ingresar
        #region Solicitar Longitud
        public void SolicitarLongitudArreglo()
        {
            int auxiliarLongitud;
            string numeroUsuario;

            do
            {
                do
                {
                    Console.Write("Ingrese la longitud del arreglo de edades: ");
                    numeroUsuario = Console.ReadLine();

                    if (numeroUsuario == "")
                    {
                        Console.WriteLine("\nEl campo no debe estar vacío, intente de nuevo...\n");
                    }

                } while (numeroUsuario == "");

                auxiliarLongitud = int.Parse(numeroUsuario);

                if (auxiliarLongitud <= 0)
                {
                    Console.WriteLine("\nEl valor debe ser mayor a 0, ingrese un valor correcto...\n");
                }

            } while (auxiliarLongitud <= 0);

            edades = new int[auxiliarLongitud];
        }
        #endregion

        //Método que solicita las edades
        public void IngresarDatosUsuario()
        {
            for (int i = 0; i < edades.Length; i++)
            {
                Console.Write($"Ingrese una edad en la posición[{i}]: ");
                edades[i] = int.Parse(Console.ReadLine());
            }
        }

        //Método que ordena el arreglo de mayor a menor
        private int[] OrdenarMayorMenor(int[] edades)
        {
            for (int i = 0; i < edades.Length; i++)
            {
                for (int j = 0; j < edades.Length - 1; j++)
                {
                    if (edades[i] > edades[j])
                    {
                        int auxEdades = edades[i];
                        edades[i] = edades[j];
                        edades[j] = auxEdades;
                    }
                }
            }

            return edades;
        }

        //Método que ordena el arreglo de menor a mayor
        public int[] OrdenarMenorMayor(int[] edades)
        {
            for (int i = 0; i < edades.Length; i++)
            {
                for (int j = 0; j < edades.Length - 1; j++)
                {
                    if (edades[i] < edades[j])
                    {
                        int auxEdades = edades[i];
                        edades[i] = edades[j];
                        edades[j] = auxEdades;
                    }
                }
            }

            return edades;
        }

        //Método que obtiene la cantidad de edades mayores o iguales a 18
        public int ObtenerMayoresEdad()
        {
            int mayoresEdad = 0;

            for (int j = 0; j < edades.Length; j++)
            {
                if (edades[j] >= 18)
                {
                    mayoresEdad++;
                }
            }

            return mayoresEdad;
        }

        public void MostrarInformacion()
        {
            if (opcionOrden == 1)
            {
                Console.WriteLine("\n********ARREGLO ORDENADO DE MAYOR A MENOR********");
            }
            else
            {
                Console.WriteLine("\n********ARREGLO ORDENADO DE MENOR A MAYOR********");
            }

            for (int k = 0; k < edades.Length; k++)
            {
                Console.WriteLine($"Edad[{k}] = {edades[k]}");
            }
        }

        //Método que muestra el arreglo ordenado deacuerdo a la opción elegida por el usuario
        public void Menu()
        {
            Console.WriteLine("\n************Menú************");
            Console.WriteLine("(1)Ordenar de Mayor a Menor");
            Console.WriteLine("(2)Ordenar de Menor a Mayor");
            Console.WriteLine("****************************");
            Console.Write("Seleccione una opción: ");
            opcionOrden = int.Parse(Console.ReadLine());

            switch (opcionOrden)
            {
                case 1:

                    OrdenarMayorMenor(edades);

                    MostrarInformacion();

                    break;

                case 2:

                    OrdenarMenorMayor(edades);

                    MostrarInformacion();

                    break;
            }

            Console.WriteLine($"\nCantidad >= 18: {ObtenerMayoresEdad()}");

            //    Console.WriteLine($"Tamaño de la lista {lista.Count}");

            //    lista.ForEach(Imprimir);

            //    Console.WriteLine("\n");

            //    foreach (var list in lista)
            //    {
            //        if (lista.IndexOf(list) > 2)
            //        {
            //            Console.WriteLine(list);
            //        }
            //    }
            Console.Write("\nPresione cualquier tecla para salir...");

            Console.ReadKey();
        }

        //public void Imprimir(object lista)
        //{
        //    Console.WriteLine(lista);
        //}
    }
}